#!/bin/bash
set -exuo pipefail

apt-get update

PACKAGES=("git" "clang-13" "libclang-dev" "cmake" "make") # "gcc-multilib" "g++-multilib")
case "${TARGET}" in
    aarch64-unknown-linux-gnu|aarch64-unknown-linux-musl)
        apt-get install -yqq "${PACKAGES[@]}" {binutils,g++,gcc}-aarch64-linux-gnu
    ;;
    arm-unknown-linux-gnueabi|arm-unknown-linux-musleabi)
        apt-get install -yqq "${PACKAGES[@]}" {binutils,g++,gcc}-arm-linux-gnueabi linux-libc-dev-armel-cross
    ;;
    arm-unknown-linux-gnueabihf|arm-unknown-linux-musleabihf)
        apt-get install -yqq "${PACKAGES[@]}" {binutils,g++,gcc}-arm-linux-gnueabihf linux-libc-dev-armhf-cross
    ;;
    armv7-unknown-linux-gnueabihf|armv7-unknown-linux-musleabihf)
        apt-get install -yqq "${PACKAGES[@]}" {binutils,g++,gcc}-arm-linux-gnueabihf linux-libc-dev-armel-cross
    ;;
    x86_64-unknown-linux-musl)
        apt-get install -yqq "${PACKAGES[@]}" musl-tools
    ;;
    x86_64-unknown-linux-gnu)
        apt-get install -yqq "${PACKAGES[@]}" binutils g++ gcc
    ;;
    *)
        apt-get install -yqq "${PACKAGES[@]}"
    ;;
esac
