#!/bin/bash -exuo pipefail
RUSTFLAGS="-C link-arg=-s"

if [[ "${TARGET}" == *musl ]]; then 
    RUSTFLAGS="${flags} -C target-feature=-crt-static"
fi

export RUSTFLAGS="${RUSTFLAGS}"

target_debian=""
case "${TARGET}" in
    aarch64-unknown-linux-gnu|aarch64-unknown-linux-musl)
        target_debian="aarch64-linux-gnu"
    ;;
    arm-unknown-linux-gnueabi|arm-unknown-linux-musleabi)
        target_debian="arm-linux-gnueabi"
    ;;
    arm-unknown-linux-gnueabihf|arm-unknown-linux-musleabihf)
        target_debian="arm-linux-gnueabihf"
    ;;
    armv7-unknown-linux-gnueabihf|armv7-unknown-linux-musleabihf)
        target_debian="arm-linux-gnueabihf"
    ;;
esac

if [[ "${target_debian}" != "" ]]; then
    export CC="${target_debian}-gcc"
    export CCX="${target_debian}-g++"
    export CPATH="/usr/${target_debian}/include"
fi

export CARGO_BUILD_TARGET="${TARGET}"
